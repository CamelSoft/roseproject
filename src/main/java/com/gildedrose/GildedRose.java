package com.gildedrose;

import com.gildedrose.Item;

public class GildedRose {

	private static final String SULFURAS_NAME = "Sulfuras, Hand of Ragnaros";
	private static final String BACKSTAGE_PASSES_NAME = "Backstage passes to a TAFKAL80ETC concert";
	private static final String AGED_BRIE_NAME = "Aged Brie";
	private static final String CONJURED = "Conjured Mana Cake";
	
    public static final int MAX_QUALITY = 50;
    public static final int MIN_QUALITY = 0;
    
	private Item[] items;

	public GildedRose(Item[] items) {
		this.setItems(items);
	}

	public void updateQuality() {
		for (Item item : getItems()) {
			if (SULFURAS_NAME.equals(item.name)) {
				continue;
			}

			item.sellIn = item.sellIn - 1;

			if (AGED_BRIE_NAME.equals(item.name)) {
				updateAgedBrie(item);
			} else if (BACKSTAGE_PASSES_NAME.equals(item.name)) {
				updateBackstagePass(item);
			} else if (CONJURED.equals(item.name)) {
				updateConjuredItem(item);
			} else {
				updateNormalItem(item);
			}
		}
	}

	private void updateAgedBrie(Item item) {
		increaseQuality(item);

		if (item.sellIn < 0) {
			increaseQuality(item);
		}
	}

	private void updateBackstagePass(Item item) {
		increaseQuality(item);

		if (item.sellIn < 10) {
			increaseQuality(item);
		}

		if (item.sellIn < 5) {
			increaseQuality(item);
		}
 
		if (item.sellIn < 0) {
			item.quality = MIN_QUALITY;
		}
	}

	private void updateConjuredItem(Item item) {
		decreaseQuality(item, 1);

		if (item.sellIn < 0) {
			decreaseQuality(item, 2);
		}
	}
	
	private void updateNormalItem(Item item) {
		decreaseQuality(item, 1);

		if (item.sellIn < 0) {
			decreaseQuality(item, 1);
		}
	}

	private void increaseQuality(Item item) {
		if (item.quality < MAX_QUALITY) {
			item.quality = item.quality + 1;
		}
	}

	private void decreaseQuality(Item item, int decrement) {
		if (item.quality > MAX_QUALITY) {
			item.quality = item.quality - decrement;
		}

	}

	public Item[] getItems() {
		return items;
	}

	public void setItems(Item[] items) {
		this.items = items;
	}
}
