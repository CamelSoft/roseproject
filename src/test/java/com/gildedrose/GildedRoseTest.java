package com.gildedrose;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GildedRoseTest {

	@Test
	public void conjuredItemsDegradeAtTwiceTheRateOfStandardItems() {
		// PREPARE
		Item item = new Item("Conjured Mana Cake", 5, 10);
		Item[] items = new Item[] { item };
		GildedRose app = new GildedRose(items);
		// EXECUTE
		app.updateQuality();
		// VERIFY
		assertEquals(10, item.quality);
	}

	@Test
	public void updateQuality_decrementsItemsQualityAndSellInBy1() {
		Item item = new Item("foo", 1, 1);
		Item[] items = new Item[] { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals("foo", item.name);
		assertEquals(1, item.quality);
		assertEquals(0, item.sellIn);
	}

	@Test
	public void onceTheSellByDateHasPassed_QualityDegradesTwiceAsFast() throws Exception {
		Item item = new Item("bar", -1, 2);
		Item[] items = new Item[] { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals("bar", item.name);
		assertEquals(2, item.quality);
		assertEquals(-2, item.sellIn);
	}

	@Test
	public void qualityOfAnItemIsNeverNegative() throws Exception {
		Item item = new Item("foo", 2, 2);
		Item[] items = new Item[] { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		assertEquals("foo", item.name);
		assertEquals(2, item.quality);
		assertEquals(-1, item.sellIn);
	}

	@Test
	public void theQualityOfAnItemIsNeverMoreThan50() throws Exception {
		Item item1 = new Item("Aged Brie", -1, 50);
		Item item2 = new Item("Backstage passes to a TAFKAL80ETC concert", 3, 49);
		Item[] items = new Item[] { item1, item2 };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals("Aged Brie", item1.name);
		assertEquals(50, item1.quality);
		assertEquals(-2, item1.sellIn);

		assertEquals("Backstage passes to a TAFKAL80ETC concert", item2.name);
		assertEquals(50, item2.quality);
		assertEquals(2, item2.sellIn);
	}

	@Test
	public void briesQualityIncrementsByOneForEachDayPastItsSellByDate() throws Exception {
		Item item = new Item("Aged Brie", -1, 1);
		Item[] items = new Item[] { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals("Aged Brie", item.name);
		assertEquals(3, item.quality);
		assertEquals(-2, item.sellIn);
	}

	@Test
	public void SulfurasBeingALegendaryItemNeverHasToBeSoldOrDecreasesInQuality() throws Exception {
		Item item = new Item("Sulfuras, Hand of Ragnaros", -1, 80);
		Item[] items = new Item[] { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals("Sulfuras, Hand of Ragnaros", item.name);
		assertEquals(80, item.quality);
		assertEquals(-1, item.sellIn);
	}

	@Test
	public void backstagePassesQualityIncrementsByOneWithEachDayPassing() throws Exception {
		Item item = new Item("Backstage passes to a TAFKAL80ETC concert", 11, 30);
		Item[] items = new Item[] { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals("Backstage passes to a TAFKAL80ETC concert", item.name);
		assertEquals(31, item.quality);
		assertEquals(10, item.sellIn);
	}

	@Test
	public void backstagePassesIncreaseInQualityBy2WhenThereAre10DaysOrLessRemaining() throws Exception {
		Item item = new Item("Backstage passes to a TAFKAL80ETC concert", 10, 30);
		Item[] items = new Item[] { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals("Backstage passes to a TAFKAL80ETC concert", item.name);
		assertEquals(32, item.quality);
		assertEquals(9, item.sellIn);
	}

	@Test
	public void backstagePassesIncreaseInQualityBy3WhenThereAre5DaysOrLessRemaining() throws Exception {
		Item item = new Item("Backstage passes to a TAFKAL80ETC concert", 5, 33);
		Item[] items = new Item[] { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals("Backstage passes to a TAFKAL80ETC concert", item.name);
		assertEquals(36, item.quality);
		assertEquals(4, item.sellIn);
	}

	@Test
	public void backstagePassesQualityDropsTo0AfterTheConcert() throws Exception {
		Item item = new Item("Backstage passes to a TAFKAL80ETC concert", -1, 30);
		Item[] items = new Item[] { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals("Backstage passes to a TAFKAL80ETC concert", item.name);
		assertEquals(0, item.quality);
		assertEquals(-2, item.sellIn);
	}

	@Test
	public void conjuredItemsDegradeInQualityTwiceAsFastAsNormalItems() throws Exception {
		Item item = new Item("Conjured", 2, 30);
		Item[] items = new Item[] { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals("Conjured", item.name);
		assertEquals(30, item.quality);
		assertEquals(1, item.sellIn);
	}

}
